This is an app for collection orientation data, gyroscope data and accelerometer data. It runs in the background.

It has three modes:

1.  Background mode that running in the background. 

2. Without image mode, which shows the sensor data on the screen.

3. With image mode, which can take a video at the same time.