package com.example.sensor_data_collection;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class AlarmReceiver extends BroadcastReceiver{
	
	AlarmManager mAlarmManager;
	@Override
	public void onReceive(Context context, Intent intent) {
		/*
		if(mAlarmManager==null){
			mAlarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		}
		Intent mServiceIntent = new Intent(context,BackgroundService.class);
		Intent mReceiverIntent = new Intent(context,AlarmReceiver.class);
		PendingIntent pi = PendingIntent.getBroadcast(context, 0, mReceiverIntent, 0);
		if(!TimeUtils.laterThanStart()){
			context.stopService(mServiceIntent);
			mAlarmManager.set(AlarmManager.RTC_WAKEUP, TimeUtils.timeToStart(), pi);
		}else if(!TimeUtils.ealierThanStop()){
			context.stopService(mServiceIntent);
			mAlarmManager.set(AlarmManager.RTC_WAKEUP, TimeUtils.timeToNextStart(), pi);
		}else {
			context.stopService(mServiceIntent);
			context.startService(mServiceIntent);
			mAlarmManager.set(AlarmManager.RTC_WAKEUP, AlarmManager.INTERVAL_HOUR, pi);
		}*/
		
		//Log.d("Background Schedule", "Lucky! "+intent.getAction());
		Intent backgroundServiceIntent = new Intent(context,BackgroundService.class);
		mAlarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		if(intent.getAction().equals("start")){
			//TODO:Stop service
			context.stopService(backgroundServiceIntent);
			mAlarmManager.cancel(BackgroundScheduleService.startPendingIntent);
			mAlarmManager.cancel(BackgroundScheduleService.runPendingIntent);
			Log.d("Background Schedule", "Stop Service:"+intent.getAction());
			mAlarmManager.set(AlarmManager.RTC_WAKEUP, TimeUtils.startTime(), BackgroundScheduleService.runPendingIntent);
			
		}
		if(intent.getAction().equals("run")){
			mAlarmManager.cancel(BackgroundScheduleService.startPendingIntent);
			mAlarmManager.cancel(BackgroundScheduleService.runPendingIntent);
			if(TimeUtils.shouldRun()){
				
				context.startService(backgroundServiceIntent);
				Log.d("Background Schedule", "Start Service:"+intent.getAction());
				mAlarmManager.set(AlarmManager.RTC_WAKEUP,System.currentTimeMillis()+TimeUtils.interval, BackgroundScheduleService.runPendingIntent);
			} else {
				context.stopService(backgroundServiceIntent);
				Log.d("Background Schedule", "Stop Service:"+intent.getAction());
				mAlarmManager.set(AlarmManager.RTC_WAKEUP, 0, BackgroundScheduleService.startPendingIntent);
			}
		}
		if(intent.getAction().equals("stop")){
			Log.d("Background Schedule", "Stop Service:"+intent.getAction());
			context.stopService(backgroundServiceIntent);
			mAlarmManager.cancel(BackgroundScheduleService.startPendingIntent);
			mAlarmManager.cancel(BackgroundScheduleService.runPendingIntent);
			//TODO:stop service
		}
	}
	
}
