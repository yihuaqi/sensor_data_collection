package com.example.sensor_data_collection;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.WindowManager.LayoutParams;
import android.widget.TextView;

public class WithoutImageViewActivity extends Activity implements SensorEventListener{
	TextView text_gyroscope_data;
	TextView text_accelerometer_data;
	
	TextView text_orientation_data;
	SensorManager mSensorManager;
	Sensor mOrientation;
	Sensor mAccelerometer;
	Sensor mGyroscope;
	
	float azimuth_angle;
	float pitch_angle;
	float roll_angle;
	float acceleration_x;
	float acceleration_y;
	float acceleration_z;
	float lin_acceleration_x;
	float lin_acceleration_y;
	float lin_acceleration_z;
	float angular_x;
	float angular_y;
	float angular_z;
	float[] accelerometerValues = new float[3];
	float[] lin_accelerometerValues = new float[3];
	float[] magneticFieldValues = new float[3];
	BufferedWriter mBufferedWriter;
	File logfile;
	static final String TAG_SENSOR = "sensor";
	static final String TAG_STACK_TRACE = "stack_trace";
	static final String TAG_DIRECTORY = "directory";
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		getWindow().addFlags(LayoutParams.FLAG_KEEP_SCREEN_ON);
		setContentView(R.layout.without_image_view);
		findAllViews();

		
		initSensors();
		initBufferedWriter();
		
	}
	
	private void initBufferedWriter() {
		String timeStamp = (new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date()));
		File subdir = new File(Environment.getExternalStorageDirectory().getAbsolutePath()+Setting.DATA_DIRECTORY);
		Log.d("File location", Environment.getExternalStorageDirectory().getAbsolutePath()+Setting.DATA_DIRECTORY);
		if(!subdir.exists()){
			subdir.mkdir();
			Log.d("File location","Make dir:"+ subdir.getAbsolutePath());
		}
		
		logfile = new File(subdir,"datalog_"+timeStamp);
		
		if (!logfile.exists()){
			try{
				logfile.createNewFile();
				Log.d("File location","Make log file:"+ logfile.getAbsolutePath());
			}
			catch(IOException e){
				e.printStackTrace();
				Log.d("File location", "Faile to make log file..");
			}
		}
		
		try {
			mBufferedWriter = new BufferedWriter(new FileWriter(logfile,true));
			if(mBufferedWriter!=null){
				Log.d(TAG_STACK_TRACE, "INITIALIZE mBufferedWriter");
			} else {
				Log.d(TAG_STACK_TRACE, "Failed: INITIALIZE mBufferedWriter");
			}
		} catch (IOException e) {
			Log.d(TAG_STACK_TRACE, "EXCEPTION?");
			e.printStackTrace();
		}
		
	}

	private void initSensors() {
		mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
		if(Setting.sensor_accelerometer){
			mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
			Log.d("Sensor register", "Accelerometer");
		} else {
			text_accelerometer_data.setVisibility(View.GONE);
			
		}
		if(Setting.sensor_gyroscope){
			mGyroscope = mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
			Log.d("Sensor register", "Gyroscope");
		} else {
			text_gyroscope_data.setVisibility(View.GONE);
		}
		if(Setting.sensor_orientation){
			mOrientation = mSensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION);
			Log.d("Sensor register", "Orientation");
			//Test
			//mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
			//mMagneticField = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
			//text_gyroscope_data.setVisibility(View.VISIBLE);
			
			
		} else {
			text_orientation_data.setVisibility(View.GONE);
		}
		
	}

	private void findAllViews() {
		text_accelerometer_data = (TextView) findViewById(R.id.text_accelerometer_data);
		
		text_gyroscope_data = (TextView) findViewById(R.id.text_gyroscope_data);
		text_orientation_data = (TextView) findViewById(R.id.text_orientation_data);
		
	}

	public void onSensorChanged(SensorEvent event) {
		//Log.d(TAG_SENSOR, "Sensor Change");
		
		//Test
		/*
		if(event.sensor==mMagneticField){
			magneticFieldValues=event.values;
		}
		if(event.sensor==mAccelerometer){
			accelerometerValues=event.values;
		}
		
		*/
		/*
		if(event.sensor==mLinearSensor){

			
			lin_acceleration_x = event.values[0];
			lin_acceleration_y = event.values[1];
			lin_acceleration_z = event.values[2];
			
			text_lin_accelerometer_data.setText("lin_acceleration_x: "+lin_acceleration_x+"\n"
										+"lin_acceleration_y: "+lin_acceleration_y+"\n"
										+"lin_acceleration_z: "+lin_acceleration_z);
			
			try {
				mBufferedWriter.append("lin,");
				mBufferedWriter.append(
						String.format("%06d, %6.3f, %6.3f, %6.3f", 
								event.timestamp,
								lin_acceleration_x,
								lin_acceleration_y,
								lin_acceleration_z));
				mBufferedWriter.newLine();
			} catch (Exception e) {
				//e.printStackTrace();
			}
		
		}		
		*/
		
		if(event.sensor==mAccelerometer){

			
			acceleration_x = event.values[0];
			acceleration_y = event.values[1];
			acceleration_z = event.values[2];
			
			text_accelerometer_data.setText("acceleration_x: "+acceleration_x+"\n"
										+"acceleration_y: "+acceleration_y+"\n"
										+"acceleration_z: "+acceleration_z);
			
			try {
				Log.d("File location", "Trying to write something");
				mBufferedWriter.append("acc,");
				mBufferedWriter.append(
						String.format("%06d, %6.3f, %6.3f, %6.3f", 
								event.timestamp,
								acceleration_x,
								acceleration_y,
								acceleration_z));
				mBufferedWriter.newLine();
			} catch (Exception e) {
				//e.printStackTrace();
				Log.d("File location", "But writing failed....why?");
			}
		
		}
		//Test
		//calculateOrientation();
		
		
		if(event.sensor==mOrientation){
			
			
			azimuth_angle = event.values[0];
			pitch_angle = event.values[1];
			roll_angle = event.values[2];
			
			text_orientation_data.setText("azimuth: "+azimuth_angle+"\n"
										+"pitch: "+pitch_angle+"\n"
										+"roll: "+roll_angle);
									
			
			try {
				mBufferedWriter.append("ori,");
				mBufferedWriter.append(
						String.format("%06d, %6.3f, %6.3f, %6.3f", 
								event.timestamp,
								azimuth_angle,
								pitch_angle,
								roll_angle));
				mBufferedWriter.newLine();
			} catch (Exception e) {
				//e.printStackTrace();
			}
		}
		if(event.sensor==mGyroscope){
			angular_x = event.values[0];
			angular_y = event.values[1];
			angular_z = event.values[2];
			
			text_gyroscope_data.setText("angular_x: "+angular_x+"\n"
					+"angular_y: "+angular_y+"\n"
					+"angular_z: "+angular_z);
			try {
				mBufferedWriter.append("gyr,");
				mBufferedWriter.append(
						String.format("%06d, %6.3f, %6.3f, %6.3f", 
								event.timestamp,
								angular_x,
								angular_y,
								angular_z));
				mBufferedWriter.newLine();
				
			} catch (Exception e) {
				//e.printStackTrace();
			}
		}
	} 
	
	void calculateOrientation(){
		float[] values = new float[3];
		float[] R = new float[9];
		SensorManager.getRotationMatrix(R, null, accelerometerValues, magneticFieldValues);
		SensorManager.getOrientation(R, values);
		
		values[0] = (float) Math.toDegrees(values[0]);
		values[1] = (float) Math.toDegrees(values[1]);
		values[2] = (float) Math.toDegrees(values[2]);
		
		azimuth_angle = values[0];
		pitch_angle = values[1];
		roll_angle = values[2];
		
		if(azimuth_angle+pitch_angle+roll_angle==0.0f){
			Log.d(TAG_SENSOR, "Error: acc:"+accelerometerValues[0]+" "+accelerometerValues[1]+" "+accelerometerValues[2]);
			Log.d(TAG_SENSOR, "Error: mag:"+magneticFieldValues[0]+" "+magneticFieldValues[1]+" "+magneticFieldValues[2]);
		} else {
			Log.d(TAG_SENSOR, "acc:"+accelerometerValues[0]+" "+accelerometerValues[1]+" "+accelerometerValues[2]);
			Log.d(TAG_SENSOR, "mag:"+magneticFieldValues[0]+" "+magneticFieldValues[1]+" "+magneticFieldValues[2]);
		}
		text_gyroscope_data.setText("azimuth_r: "+azimuth_angle+"\n"
									+"pitch_r: "+pitch_angle+"\n"
									+"roll_r: "+roll_angle);
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		mSensorManager.unregisterListener(this);
		

		try {
			if(mBufferedWriter!=null){
				mBufferedWriter.close();
			} else {
				Log.d(TAG_STACK_TRACE, "mBufferedWriter is NULL");
			}
		} catch (IOException e) {
			//e.printStackTrace();
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		registerSensors();
		
	}

	private void registerSensors() {
		if(mOrientation!=null){
			mSensorManager.registerListener(this, mOrientation,
					Setting.sensor_frequency);
			//Log.d(TAG_SENSOR, "Registered Orientation");
			
			
			//Test
			//mSensorManager.registerListener(this,mAccelerometer,Setting.sensor_frequency);
			//mSensorManager.registerListener(this,mMagneticField,Setting.sensor_frequency);
		}
		if(mAccelerometer!=null){
			mSensorManager.registerListener(this, mAccelerometer, 
					Setting.sensor_frequency);
			
			
		}
		if(mGyroscope!=null){
			mSensorManager.registerListener(this, mGyroscope, Setting.sensor_frequency);
		}
		
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		// TODO Auto-generated method stub
		
	}
}
