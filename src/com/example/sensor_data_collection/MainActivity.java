package com.example.sensor_data_collection;

import android.os.Bundle;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.TimePicker;

import com.example.sensor_data_collection.*;


public class MainActivity extends Activity implements OnClickListener{
	Button button_setting;
	Button button_start;
	Button button_stop;
	Button button_mode;
	Button button_sensor;
	Button button_setting_back;
	Button button_setting_mode_back;
	Button button_setting_sensor_back;
	Button button_bluetooth_test;
	Button button_setting_timer;
	Button button_setting_timer_back;
	
	TimePicker timepicker_setting_timer_start;
	TimePicker timepicker_setting_timer_stop;
	
	LinearLayout layout_main;
	LinearLayout layout_main_setting;
	LinearLayout layout_main_setting_mode;
	LinearLayout layout_main_setting_sensor;
	LinearLayout layout_main_setting_timer;
	RadioButton radio_withImage;
	RadioButton radio_withoutImage;
	RadioButton radio_background;
	RadioButton radio_frequency_high;
	RadioButton radio_frequency_mid;
	RadioButton radio_frequency_low;
	CheckBox check_savingimage;
	CheckBox check_gyroscope;
	CheckBox check_accelerometer;
	CheckBox check_orientation;
	
	static final String TAG_SHAREDPREFS = "share";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		findAllViews();
		setButtonListeners();
		
	}
	
	// Set onClickListener to all buttons
	void setButtonListeners() {
		button_setting.setOnClickListener(this);
		button_start.setOnClickListener(this);
		button_stop.setOnClickListener(this);
		button_mode.setOnClickListener(this);
		button_sensor.setOnClickListener(this);
		button_setting_back.setOnClickListener(this);
		button_setting_mode_back.setOnClickListener(this);
		button_setting_sensor_back.setOnClickListener(this);
		button_bluetooth_test.setOnClickListener(this);
		button_setting_timer.setOnClickListener(this);
		button_setting_timer_back.setOnClickListener(this);
		
		
	}
	
	// Find all views by its id.
	void findAllViews(){
		button_setting = (Button) findViewById(R.id.button_setting);
		button_start = (Button) findViewById(R.id.button_start);
		button_stop = (Button) findViewById(R.id.button_stop);
		button_mode = (Button) findViewById(R.id.button_mode);
		button_sensor = (Button) findViewById(R.id.button_sensor);
		button_setting_back = (Button) findViewById(R.id.button_setting_back);
		button_setting_mode_back = (Button) findViewById(R.id.button_setting_mode_back);
		button_setting_sensor_back = (Button) findViewById(R.id.button_setting_sensor_back);
		button_bluetooth_test = (Button) findViewById(R.id.button_bluetooth_test);
		
		layout_main = (LinearLayout) findViewById(R.id.layout_main);
		layout_main_setting = (LinearLayout) findViewById(R.id.layout_main_setting);
		layout_main_setting_mode = (LinearLayout) findViewById(R.id.layout_main_setting_mode);
		layout_main_setting_sensor = (LinearLayout) findViewById(R.id.layout_main_setting_sensor);
		layout_main_setting_timer = (LinearLayout) findViewById(R.id.layout_main_setting_timer);
		radio_withImage = (RadioButton) findViewById(R.id.radio_withimage);
		radio_withoutImage = (RadioButton) findViewById(R.id.radio_withoutimage);
		radio_background = (RadioButton) findViewById(R.id.radio_background);
		radio_frequency_high = (RadioButton) findViewById(R.id.radio_frequency_high);
		radio_frequency_low = (RadioButton) findViewById(R.id.radio_frequency_low);
		radio_frequency_mid = (RadioButton) findViewById(R.id.radio_frequency_mid);
		check_accelerometer = (CheckBox) findViewById(R.id.check_accelerometer);
		check_gyroscope = (CheckBox) findViewById(R.id.check_gyrometer);
		check_orientation = (CheckBox) findViewById(R.id.check_orientation);
		check_savingimage = (CheckBox) findViewById(R.id.check_saveimage);
		button_setting_timer = (Button) findViewById(R.id.button_timer);
		button_setting_timer_back = (Button) findViewById(R.id.button_setting_timer_back);
		
		timepicker_setting_timer_start =  (TimePicker) findViewById(R.id.timepicker_setting_timer_start);
		//timepicker_setting_timer_start.setIs24HourView(true);
		timepicker_setting_timer_stop = (TimePicker) findViewById(R.id.timepicker_setting_timer_stop);
		//timepicker_setting_timer_stop.setIs24HourView(true);
		
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	@Override
	// UI changing logic.
	public void onClick(View arg0) {
		switch(arg0.getId()){
		case R.id.button_mode:
			layout_main_setting.setVisibility(View.GONE);
			layout_main_setting_mode.setVisibility(View.VISIBLE);
			break;
		case R.id.button_sensor:
			layout_main_setting.setVisibility(View.GONE);
			layout_main_setting_sensor.setVisibility(View.VISIBLE);
			break;
		case R.id.button_setting:
			layout_main.setVisibility(View.GONE);
			layout_main_setting.setVisibility(View.VISIBLE);
			break;
		case R.id.button_setting_back:
			layout_main_setting.setVisibility(View.GONE);
			layout_main.setVisibility(View.VISIBLE);
			break;
		case R.id.button_setting_mode_back:
			layout_main_setting_mode.setVisibility(View.GONE);
			layout_main_setting.setVisibility(View.VISIBLE);
			break;
		case R.id.button_setting_sensor_back:
			layout_main_setting_mode.setVisibility(View.GONE);
			layout_main_setting.setVisibility(View.VISIBLE);
			break;
		case R.id.button_timer:
			layout_main_setting.setVisibility(View.GONE);
			layout_main_setting_timer.setVisibility(View.VISIBLE);
			break;
		case R.id.button_setting_timer_back:
			layout_main_setting.setVisibility(View.VISIBLE);
			layout_main_setting_timer.setVisibility(View.GONE);
			int startHour = timepicker_setting_timer_start.getCurrentHour();
			int startMinutes = timepicker_setting_timer_start.getCurrentMinute();
			Setting.serviceStartHour = startHour;
			Setting.serviceStartMin = startMinutes;
			int stopHour = timepicker_setting_timer_stop.getCurrentHour();
			int stopMinute = timepicker_setting_timer_stop.getCurrentMinute();
			Setting.serviceStopHour = stopHour;
			Setting.serviceStopMin = stopMinute;
			break;
		
		case R.id.button_start:
			Intent startMeasureIntent;
			switch (Setting.mode) {
			case Setting.MODE_WITHOUT_IMAGE_VIEW:
				startMeasureIntent = new Intent(this,WithoutImageViewActivity.class);
				startActivity(startMeasureIntent);				
				break;
			case Setting.MODE_BACKGROUND:
				button_start.setVisibility(View.GONE);
				button_stop.setVisibility(View.VISIBLE);
				//startService(new Intent(this,BackgroundService.class));
				startService(new Intent(this,BackgroundScheduleService.class));
				Setting.isRunning = true;
				break;
			case Setting.MODE_WITH_IMAGE_VIEW:
				startMeasureIntent = new Intent(this,WithImageViewActivity.class);
				startActivity(startMeasureIntent);
				break;
			default:
				break;
			}
			break;
		case R.id.button_stop:
			//stopService(new Intent(this,BackgroundService.class));
			stopService(new Intent(this,BackgroundScheduleService.class));
			button_stop.setVisibility(View.GONE);
			button_start.setVisibility(View.VISIBLE);
			Setting.isRunning = false;
			break;
		case R.id.button_bluetooth_test:
			startActivity(new Intent(this,BluetoothTestActivity.class));
			
			break;
		

		
		}
	}
	@Override
	protected void onPause() {
		savePreference();
		
		super.onPause();
		
	}

	// Save setting parameters into SharedPreferences 
	void savePreference(){
		SharedPreferences.Editor editor = getPreferences(MODE_PRIVATE).edit();
		editor.putInt(Setting.KEY_MODE, Setting.mode);
		Log.d(TAG_SHAREDPREFS, "PUT KEY_MODE = "+Setting.mode);
		editor.putBoolean(Setting.KEY_SAVEIMAGE, Setting.saveImage);
		Log.d(TAG_SHAREDPREFS,"PUT KEY_SAVIMAGE = "+Setting.saveImage);
		editor.putBoolean(Setting.KEY_SENSOR_ACCELEROMETER, Setting.sensor_accelerometer);
		Log.d(TAG_SHAREDPREFS, "PUT KEY_SENSOR_ACCELEROMETER = "+Setting.sensor_accelerometer);
		editor.putInt(Setting.KEY_SENSOR_FREQUENCY, Setting.sensor_frequency);
		Log.d(TAG_SHAREDPREFS, "PUT KEY_SENSOR_FREQUENCY = "+Setting.sensor_frequency);
		editor.putBoolean(Setting.KEY_SENSOR_GYROSCOPE, Setting.sensor_gyroscope);
		Log.d(TAG_SHAREDPREFS,  "PUT KEY_SENSOR_GYROSCOPE = "+Setting.sensor_gyroscope);
		editor.putBoolean(Setting.KEY_SENSOR_ORIENTATION, Setting.sensor_orientation);
		Log.d(TAG_SHAREDPREFS, "PUT KEY_SENSOR_ORIENTATION = "+Setting.sensor_orientation);
		editor.putInt(Setting.KEY_SERVICE_START_HOUR, Setting.serviceStartHour);
		editor.putInt(Setting.KEY_SERVICE_START_MIN, Setting.serviceStartMin);
		editor.putInt(Setting.KEY_SERVICE_STOP_HOUR, Setting.serviceStopHour);
		editor.putInt(Setting.KEY_SERVICE_STOP_MIN, Setting.serviceStopMin);
		editor.commit();
	}
	
	@Override
	protected void onResume() {
		loadPreference();
		renderPreferrence();
		
		super.onResume();
	}
	// Load setting from SharedPreferences
	void loadPreference(){
		SharedPreferences prefs = getPreferences(MODE_PRIVATE);
		Setting.mode = prefs.getInt(Setting.KEY_MODE, Setting.MODE_BACKGROUND);
		Log.d(TAG_SHAREDPREFS, "GET KEY_MODE = "+Setting.mode);
		Setting.saveImage = prefs.getBoolean(Setting.KEY_SAVEIMAGE, false);
		Log.d(TAG_SHAREDPREFS, "GET KEY_SAVEIMAGE = "+Setting.saveImage);
		Setting.sensor_accelerometer = prefs.getBoolean(Setting.KEY_SENSOR_ACCELEROMETER, false);
		Log.d(TAG_SHAREDPREFS, "GET KEY_SENSOR_ACCELEROMETER = "+Setting.sensor_accelerometer);
		Setting.sensor_gyroscope = prefs.getBoolean(Setting.KEY_SENSOR_GYROSCOPE, false);
		Log.d(TAG_SHAREDPREFS, "GET KEY_SENSOR_GYROSCOPE = "+Setting.sensor_gyroscope);
		Setting.sensor_orientation = prefs.getBoolean(Setting.KEY_SENSOR_ORIENTATION, false);
		Log.d(TAG_SHAREDPREFS, "GET KEY_SENSOR_ORIENTATION = "+Setting.sensor_orientation );
		Setting.sensor_frequency = prefs.getInt(Setting.KEY_SENSOR_FREQUENCY, Setting.FREQUENCY_LOW);
		Log.d(TAG_SHAREDPREFS, "GET KEY_SENSOR_FREQUENCY = "+Setting.sensor_frequency );
		Setting.serviceStartHour = prefs.getInt(Setting.KEY_SERVICE_START_HOUR, 7);
		Setting.serviceStartMin = prefs.getInt(Setting.KEY_SERVICE_START_MIN, 0);
		Setting.serviceStopHour = prefs.getInt(Setting.KEY_SERVICE_STOP_HOUR,23);
		Setting.serviceStopMin = prefs.getInt(Setting.KEY_SERVICE_STOP_MIN,0);
		
	}
	
	// Check RadioButton and CheckBox according to Setting.
	void renderPreferrence(){
		switch (Setting.mode) {
		case Setting.MODE_BACKGROUND:
			radio_background.setChecked(true);
			break;
		case Setting.MODE_WITH_IMAGE_VIEW:
			radio_withImage.setChecked(true);
			break;
		case Setting.MODE_WITHOUT_IMAGE_VIEW:
			radio_withoutImage.setChecked(true);
			break;

		default:
			break;
		}
		
		check_accelerometer.setChecked(Setting.sensor_accelerometer);
		check_gyroscope.setChecked(Setting.sensor_gyroscope);
		check_orientation.setChecked(Setting.sensor_orientation);
		check_savingimage.setChecked(Setting.saveImage);
		
		switch (Setting.sensor_frequency) {
		case Setting.FREQUENCY_HIGH:
			radio_frequency_high.setChecked(true);
			break;
		case Setting.FREQUENCY_LOW:
			radio_frequency_low.setChecked(true);
			break;
		case Setting.FREQUENCY_MID:
			radio_frequency_mid.setChecked(true);
			break;
		default:
			break;
		}
		
	
		Setting.isRunning = isMyServiceRunning();

		if(Setting.isRunning){
			button_start.setVisibility(View.GONE);
			button_stop.setVisibility(View.VISIBLE);
		}
		
		timepicker_setting_timer_start.setCurrentHour(Setting.serviceStartHour);
		timepicker_setting_timer_start.setCurrentMinute(Setting.serviceStartMin);
		timepicker_setting_timer_stop.setCurrentHour(Setting.serviceStopHour);
		timepicker_setting_timer_stop.setCurrentMinute(Setting.serviceStopMin);
	}
	private boolean isMyServiceRunning() {
	    ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
	    for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
	        if (BackgroundService.class.getName().equals(service.service.getClassName())) {
	            return true;
	        }
	    }
	    return false;
	}

	public void onCheckedBoxClicked(View view){
		switch(view.getId()){
		case R.id.check_accelerometer:
			Setting.sensor_accelerometer = check_accelerometer.isChecked();
			Log.d(TAG_SHAREDPREFS, "Checked sensor_accelerometer : "+check_accelerometer.isChecked());
			break;
		case R.id.check_gyrometer:
			Setting.sensor_gyroscope = check_gyroscope.isChecked();
			Log.d(TAG_SHAREDPREFS, "Checked sensor_gyrometer : "+check_gyroscope.isChecked());
			break;
		case R.id.check_orientation:
			Setting.sensor_orientation = check_orientation.isChecked();
			Log.d(TAG_SHAREDPREFS, "Checked sensor_orientation : "+check_orientation.isChecked());
			break;
		case R.id.check_saveimage:
			Setting.saveImage = check_savingimage.isChecked();
			Log.d(TAG_SHAREDPREFS, "Checked saveImage : "+check_savingimage.isChecked());
			break;
		}
		
	}
	public void onRadioButtonClicked(View view) {
		switch (view.getId()) {
		case R.id.radio_background:
			Setting.mode = Setting.MODE_BACKGROUND;
			Log.d(TAG_SHAREDPREFS, "Checked mode : "+Setting.MODE_BACKGROUND);
			break;
		case R.id.radio_withimage:
			Setting.mode = Setting.MODE_WITH_IMAGE_VIEW;
			Log.d(TAG_SHAREDPREFS, "Checked mode : "+Setting.MODE_WITH_IMAGE_VIEW);
			break;
		case R.id.radio_withoutimage:
			Setting.mode = Setting.MODE_WITHOUT_IMAGE_VIEW;
			Log.d(TAG_SHAREDPREFS, "Checked mode : "+Setting.MODE_WITHOUT_IMAGE_VIEW);
			break;
		case R.id.radio_frequency_high:
			Setting.sensor_frequency = Setting.FREQUENCY_HIGH;
			Log.d(TAG_SHAREDPREFS, "Checked sensor_frequency : "+Setting.FREQUENCY_HIGH);
			break;
		case R.id.radio_frequency_low:
			Setting.sensor_frequency = Setting.FREQUENCY_LOW;
			Log.d(TAG_SHAREDPREFS, "Checked sensor_frequency : "+Setting.FREQUENCY_LOW);
			break;
		case R.id.radio_frequency_mid:
			Setting.sensor_frequency = Setting.FREQUENCY_MID;
			Log.d(TAG_SHAREDPREFS, "Checked sensor_frequency : "+Setting.FREQUENCY_MID);
			break;
		default:
			break;
		}
	}

}
