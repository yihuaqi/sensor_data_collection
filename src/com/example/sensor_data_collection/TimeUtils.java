package com.example.sensor_data_collection;

import android.app.AlarmManager;
import android.text.format.Time;
import android.util.Log;

public class TimeUtils {
	
	public static long interval = 3600000;
	static public long timeToStart(){
		Time timeNow = new Time();
		timeNow.setToNow();
		Time timeStart = new Time(timeNow);
		timeStart.hour = Setting.serviceStartHour;
		timeStart.minute = Setting.serviceStartMin;
		
		return timeStart.toMillis(false) - timeNow.toMillis(false);
		
	}
	static public long timeToStop(){
		Time timeNow = new Time();
		timeNow.setToNow();
		Time timeStop = new Time(timeNow);
		timeStop.hour = Setting.serviceStopHour;
		timeStop.minute = Setting.serviceStopMin;
		
		return timeStop.toMillis(false) - timeNow.toMillis(false);
	}
	static public long timeToNextStart(){
		Time timeNow = new Time();
		timeNow.setToNow();
		Time timeStart = new Time(timeNow);
		timeStart.hour = Setting.serviceStartHour;
		timeStart.minute = Setting.serviceStartMin;
		timeStart.monthDay++;
		
		return timeStart.toMillis(false) - timeNow.toMillis(false);
	}
	static public boolean laterThanStart(){
		return timeToStart()<AlarmManager.INTERVAL_HALF_HOUR;
	}
	static public boolean ealierThanStop(){
		return timeToStop()>0;
	}
	public static long startTime() {
		Time timeNow = new Time();
		timeNow.setToNow();
		Time timeStart = new Time(timeNow);
		timeStart.hour = Setting.serviceStartHour;
		timeStart.minute = Setting.serviceStartMin;
		Time timeEnd = new Time(timeNow);
		timeEnd.hour = Setting.serviceStopHour;
		timeEnd.minute = Setting.serviceStopMin;
		if(Time.compare(timeStart, timeEnd) < 0){
			if(Time.compare(timeNow, timeEnd) < 0){
				return timeStart.toMillis(true);
			} else {
				timeStart.monthDay++;
				Log.d("Background Schedule",timeStart.hour+":"+timeStart.minute);
				return timeStart.toMillis(true);
				
			}
		}else if(Time.compare(timeStart, timeEnd) > 0){
			if(Time.compare(timeNow, timeEnd) < 0){
				timeStart.monthDay--;
				Log.d("Background Schedule", timeStart.hour+":"+timeStart.minute);
				return timeStart.toMillis(true);
			} else {
				Log.d("Background Schedule", timeStart.hour+":"+timeStart.minute);
				return timeStart.toMillis(true);
			}
		}
		return 0;
	}
	
	public static boolean shouldRun() {
		Time timeNow = new Time();
		timeNow.setToNow();
		Time timeStart = new Time(timeNow);
		timeStart.hour = Setting.serviceStartHour;
		timeStart.minute = Setting.serviceStartMin-5;
		Time timeEnd = new Time(timeNow);
		timeEnd.hour = Setting.serviceStopHour;
		timeEnd.minute = Setting.serviceStopMin+5;
		if(Time.compare(timeStart, timeEnd)<0){
			if( (Time.compare(timeNow, timeStart)>0)&&
					(Time.compare(timeNow , timeEnd)<0)){
				Log.d("Background Schedule", "Should run 1");
				return true;
			} else {
				Log.d("Background Schedule", "Shouldnt run 1");
				return false;
			}
		} else if(Time.compare(timeStart, timeEnd )>0){
			if( (Time.compare(timeNow, timeStart)>0)||
					(Time.compare(timeNow, timeEnd)<0)){
				Log.d("Background Schedule", "Should run 2");
				return true;
			} else {
				Log.d("Background Schedule", "Shouldnt run 2");
				return false;
			}
		} 
		
		return true;
	}

}
