package com.example.sensor_data_collection;

import java.util.Calendar;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.text.format.Time;
import android.util.Log;

public class BackgroundScheduleService extends Service{
	public static PendingIntent startPendingIntent;
	public static PendingIntent stopPendingIntent;
	public static PendingIntent runPendingIntent;
	public static Intent receiverIntent;
	AlarmManager mAlarmManager;
	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		return null;
	}
	PendingIntent pi;
	@Override
	public void onCreate() {
		receiverIntent = new Intent(this,AlarmReceiver.class);
		startPendingIntent = 
			PendingIntent.getBroadcast(this,
					0, 
					receiverIntent.setAction("start"), 
					0);
		stopPendingIntent = 
			PendingIntent.getBroadcast(this,
					0, 
					receiverIntent.setAction("stop"), 
					0);
		runPendingIntent = 
			PendingIntent.getBroadcast(this,
					0, 
					receiverIntent.setAction("run"), 
					0);
		mAlarmManager = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
		//Intent mIntent = new Intent(this,AlarmReceiver.class);
		//pi = PendingIntent.getBroadcast(this, 0, mIntent, 0); 
		//mAlarmManager.set(AlarmManager.RTC_WAKEUP, TimeUtils.timeToStart(), pi);
		mAlarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), startPendingIntent);
		
		Log.d("Background Schedule", "Set alarm");
		//Log.d("Background Schedule", timeToStart()+"\n"+timeToStop()+"\n"+timeToNextStart());
		super.onCreate();
	}
	@Override
	public void onDestroy() {
		mAlarmManager.set(AlarmManager.RTC_WAKEUP, 0, stopPendingIntent);
		
		super.onDestroy();
	}
	



}
