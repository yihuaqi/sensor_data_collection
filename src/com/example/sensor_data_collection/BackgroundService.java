package com.example.sensor_data_collection;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.app.IntentService;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.MediaPlayer;
import android.os.Environment;
import android.os.IBinder;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.os.Vibrator;
import android.provider.Settings;
import android.util.Log;

public class BackgroundService extends Service implements SensorEventListener{
	WakeLock wakeLock =null;
	SensorManager mSensorManager;
	Sensor mOrientation;
	Sensor mAccelerometer;
	Sensor mGyroscope; 
	ScreenOnOffReceiver mReceiver;
	float azimuth_angle;
	float pitch_angle;
	float roll_angle;
	float acceleration_x;
	float acceleration_y;
	float acceleration_z;
	float angular_x;
	float angular_y;
	float angular_z;
	static final String TAG_BACKGROUND = "background";
	BufferedWriter mBufferedWriter;
	File logfile;
	static final String TAG_STACK_TRACE = "stack_trace";
	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		// TODO Auto-generated method stub
		
	}

	public void logCollisionEvent(){
		try {
			mBufferedWriter.append("Collision,"+System.currentTimeMillis()+", 0, 0, 0");
			mBufferedWriter.newLine();
			Log.d("Broadcast Receiver", "Collision,"+System.currentTimeMillis());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
		 // Vibrate for 500 milliseconds
		 v.vibrate(500);
		 
	}
	
	public void logTripEvent(){
		try {
			mBufferedWriter.append("Trip,"+System.currentTimeMillis()+", 0, 0, 0");
			mBufferedWriter.newLine();
			Log.d("Broadcast Receiver", "Trip,"+System.currentTimeMillis());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
		 // Vibrate for 500 milliseconds
		 v.vibrate(500);
		 try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 v.vibrate(1000);
	}
	
	@Override
	public void onSensorChanged(SensorEvent event) {
		long timestamp = System.currentTimeMillis();
		Log.d(TAG_BACKGROUND, "Recording");
		if(event.sensor==mAccelerometer){
			acceleration_x = event.values[0];
			acceleration_y = event.values[1];
			acceleration_z = event.values[2];
			
			//Log.d(TAG_BACKGROUND, "Accelerometer changes");
			try {
				mBufferedWriter.append("acc,");
				mBufferedWriter.append(
						String.format("%06d, %6.3f, %6.3f, %6.3f", 
								timestamp,
								acceleration_x,
								acceleration_y,
								acceleration_z));
				mBufferedWriter.newLine();
				
			} catch (Exception e) {
				//e.printStackTrace();
			}
		}
		if(event.sensor==mOrientation){
			
			azimuth_angle = event.values[0];
			pitch_angle = event.values[1];
			roll_angle = event.values[2];

			//Log.d(TAG_BACKGROUND, "Orientation changes");
			try {
				mBufferedWriter.append("ori,");
				mBufferedWriter.append(
						String.format("%06d, %6.3f, %6.3f, %6.3f", 
								timestamp,
								azimuth_angle,
								pitch_angle,
								roll_angle));
				mBufferedWriter.newLine();
			} catch (Exception e) {
				//e.printStackTrace();
			}
		}
		if(event.sensor==mGyroscope){
			angular_x = event.values[0];
			angular_y = event.values[1];
			angular_z = event.values[2];
			
			//Log.d(TAG_BACKGROUND, "Gyroscope changes");
			try {
				mBufferedWriter.append("gyr,");
				mBufferedWriter.append(
						String.format("%06d, %6.3f, %6.3f, %6.3f", 
								timestamp,
								angular_x,
								angular_y,
								angular_z));
				mBufferedWriter.newLine();
				
			} catch (Exception e) {
				//e.printStackTrace();
			}
		}
		
		
	}
	public class ScreenOnOffReceiver extends BroadcastReceiver{
		long prevTimeStamp = 0;
		long curTimeStamp = 0;
		static final long COLLISION_THRESHOLD = 1500;
		
		static final long TRIP_THRESHOLD = 5000;
		@Override
		public void onReceive(Context context, Intent intent) {
			Log.d("Broadcast Receiver","bingo!");
			curTimeStamp = System.currentTimeMillis();
			if(curTimeStamp - prevTimeStamp < COLLISION_THRESHOLD){
				logCollisionEvent();
				prevTimeStamp = 0;
				curTimeStamp = 0;
			} else if (curTimeStamp - prevTimeStamp < TRIP_THRESHOLD){
				logTripEvent();
				prevTimeStamp = 0;
				curTimeStamp = 0;
			} else {
				prevTimeStamp = curTimeStamp;
			}
			
			
			
		}
		
	}
	@Override
	public void onCreate() {
		
		mReceiver = new ScreenOnOffReceiver();
		IntentFilter f = new IntentFilter();
		f.addAction(Intent.ACTION_SCREEN_ON);
		f.addAction(Intent.ACTION_SCREEN_OFF);
		registerReceiver(mReceiver, f);
		
		
		acquireLock();
		initSensors();
		
		registerSensor();
		Log.d(TAG_BACKGROUND, "Service Create");
		super.onCreate();
	}
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		Log.d(TAG_BACKGROUND, "Start commend");
		if(mBufferedWriter!=null){
			try {
				mBufferedWriter.flush();
				mBufferedWriter.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		initBufferedWriter();
		return START_STICKY;
	}
	@Override
	public void onDestroy() {
		Log.d(TAG_BACKGROUND, "Service Destroyed");
		if(wakeLock!=null){
			if(wakeLock.isHeld()){
				wakeLock.release();
			}
		}
		mSensorManager.unregisterListener(this);
		try {
			mBufferedWriter.flush();
			mBufferedWriter.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		unregisterReceiver(mReceiver);
		super.onDestroy();
	}
	private void initBufferedWriter() {
		String timeStamp = (new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date()));
		File subdir = new File(Environment.getExternalStorageDirectory().getAbsolutePath()+Setting.DATA_DIRECTORY);
		if(!subdir.exists()){
			subdir.mkdir();
		}
		
		logfile = new File(subdir,"datalog_"+timeStamp);
		
		if (!logfile.exists()){
			try{
				logfile.createNewFile();
				
			}
			catch(IOException e){
				e.printStackTrace();
			}
		}
		Log.d(TAG_STACK_TRACE, "why?");
		try {
			mBufferedWriter = new BufferedWriter(new FileWriter(logfile,true));
			if(mBufferedWriter!=null){
				Log.d(TAG_STACK_TRACE, "INITIALIZE mBufferedWriter");
			} else {
				Log.d(TAG_STACK_TRACE, "Failed: INITIALIZE mBufferedWriter");
			}
		} catch (IOException e) {
			Log.d(TAG_STACK_TRACE, "EXCEPTION?");
			e.printStackTrace();
		}
		
	}

	private void initSensors() {
		mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
		if(Setting.sensor_accelerometer){
			mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		}
		if(Setting.sensor_gyroscope){
			mGyroscope = mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
		}
		if(Setting.sensor_orientation){
			mOrientation = mSensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION);
		}
	}

	private void acquireLock() {
		PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
		wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
		        "MyWakelockTag");
		wakeLock.acquire();
		
	}




	private void registerSensor() {
		if(mOrientation!=null){
			mSensorManager.registerListener(this, mOrientation,
					Setting.sensor_frequency);
			//Log.d(TAG_SENSOR, "Registered Orientation");
		}
		if(mAccelerometer!=null){
			mSensorManager.registerListener(this, mAccelerometer, 
					Setting.sensor_frequency);
			
		}
		if(mGyroscope!=null){
			mSensorManager.registerListener(this, mGyroscope, Setting.sensor_frequency);
		}
		
	}

}
